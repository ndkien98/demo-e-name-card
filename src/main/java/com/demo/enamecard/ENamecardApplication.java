package com.demo.enamecard;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ENamecardApplication {

    public static void main(String[] args) {
        SpringApplication.run(ENamecardApplication.class, args);
    }

}
