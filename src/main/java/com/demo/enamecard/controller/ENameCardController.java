package com.demo.enamecard.controller;

import com.demo.enamecard.model.request.CreateENameCardRequest;
import com.demo.enamecard.model.response.ENameCardResponse;
import com.demo.enamecard.model.response.SystemResponse;
import com.demo.enamecard.services.ENameCardService;
import org.springframework.data.domain.PageImpl;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.PositiveOrZero;


@RestController
@RequestMapping("/e-name-card")
@Validated
public class ENameCardController {

    private ENameCardService service;

    public ENameCardController(ENameCardService eNameCardService){
        this.service = eNameCardService;
    }

    @GetMapping()
    ResponseEntity<SystemResponse<PageImpl<ENameCardResponse>>> getList( @PositiveOrZero @RequestParam(required = false,defaultValue = "0") int page,
                                                                         @PositiveOrZero @RequestParam(required = false,defaultValue = "1") int size){
        return ResponseEntity.ok(service.getList(page, size));
    }

    @PostMapping()
    ResponseEntity<SystemResponse> create(@RequestBody CreateENameCardRequest createENameCardRequest){
        return ResponseEntity.ok(service.create(createENameCardRequest));
    }

}
