package com.demo.enamecard.repositories;

import com.demo.enamecard.model.entities.ENameCardEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ENameCardRepository extends JpaRepository<ENameCardEntity,Integer> {
}
