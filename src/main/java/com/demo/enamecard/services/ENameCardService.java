package com.demo.enamecard.services;

import com.demo.enamecard.model.request.CreateENameCardRequest;
import com.demo.enamecard.model.response.ENameCardResponse;
import com.demo.enamecard.model.response.SystemResponse;
import org.springframework.data.domain.PageImpl;


public interface ENameCardService {

    SystemResponse<PageImpl<ENameCardResponse>> getList(int page, int size);

    SystemResponse create(CreateENameCardRequest createENameCardRequest);
}
