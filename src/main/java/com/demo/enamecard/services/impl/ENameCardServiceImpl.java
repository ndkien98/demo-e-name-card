package com.demo.enamecard.services.impl;

import com.demo.enamecard.mapper.ENameCardMapper;
import com.demo.enamecard.model.entities.ENameCardEntity;
import com.demo.enamecard.model.request.CreateENameCardRequest;
import com.demo.enamecard.model.response.ENameCardResponse;
import com.demo.enamecard.model.response.SystemResponse;
import com.demo.enamecard.repositories.ENameCardRepository;
import com.demo.enamecard.services.ENameCardService;
import com.demo.enamecard.utils.HttpCodeResponse;
import com.demo.enamecard.utils.StringResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ENameCardServiceImpl implements ENameCardService {

    private ENameCardRepository repository;


    @Autowired
    public ENameCardServiceImpl(ENameCardRepository eNameCardRepository){
        repository = eNameCardRepository;
    }


    @Override
    public SystemResponse<PageImpl<ENameCardResponse>> getList(int page, int size) {

        Pageable pageable = PageRequest.of(page, size);
        Page eNameCardPage = repository.findAll(pageable);
        List<ENameCardResponse> eNameCardResponses = ENameCardMapper.INSTANCE.mapList(eNameCardPage.getContent());

        return new SystemResponse<>(HttpCodeResponse.SUCCESS, StringResponse.OK,new PageImpl<>(eNameCardResponses,pageable,eNameCardPage.getTotalElements()));
    }

    @Override
    public SystemResponse<ENameCardResponse> create(CreateENameCardRequest createENameCardRequest) {
        ENameCardEntity eNameCardEntity = ENameCardMapper.INSTANCE.map(createENameCardRequest);

        repository.save(eNameCardEntity);
        return new SystemResponse<>(HttpCodeResponse.SUCCESS,StringResponse.OK);
    }
}
