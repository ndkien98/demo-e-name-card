package com.demo.enamecard.exceptions;

import com.demo.enamecard.model.response.SystemResponse;
import com.demo.enamecard.utils.HttpCodeResponse;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Path;

@ControllerAdvice
public class ExceptionAdvice {
  public static final String ERROR_MESSGE_FORMAT = "%s: %s";

  @ExceptionHandler(ConstraintViolationException.class)
  @ResponseStatus(HttpStatus.BAD_REQUEST)
  @ResponseBody
  public SystemResponse onConstraintValidationException(
    ConstraintViolationException e) {
    ConstraintViolation<?> firstViolation = e.getConstraintViolations().iterator().next();
    Path propertyPath = firstViolation.getPropertyPath();
    return new SystemResponse(HttpCodeResponse.BAD_REQUEST, String.format(ERROR_MESSGE_FORMAT,
            propertyPath.toString().substring(propertyPath.toString().lastIndexOf(".") + 1),
            firstViolation.getMessage()));
  }
}
