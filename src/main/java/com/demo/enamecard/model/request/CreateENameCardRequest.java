package com.demo.enamecard.model.request;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
public class CreateENameCardRequest {

    private String avatar;

    private String fullName;

    private String phone;

    @Email
    private String email;

    private String faceBookLink;

    private String positions;

}
