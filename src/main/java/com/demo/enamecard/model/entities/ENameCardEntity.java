package com.demo.enamecard.model.entities;

import lombok.Data;

import javax.persistence.*;
import java.time.OffsetDateTime;

@Entity
@Table(name = "e_name_card")
@Data
public class ENameCardEntity {

    @Id
    @SequenceGenerator(name = "e_name_card_id_seq",sequenceName = "e_name_card_id_seq",allocationSize = 1, initialValue = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE,generator = "e_name_card_id_seq")
    private Integer id;

    private String avatar;

    private String fullName;

    private String phone;

    private String email;

    private String faceBookLink;

    private String positions;

    @Column(columnDefinition = "timestamp default now()")
    private OffsetDateTime createdAt = OffsetDateTime.now();

    private OffsetDateTime updatedAt;

}
