package com.demo.enamecard.mapper;

import com.demo.enamecard.model.entities.ENameCardEntity;
import com.demo.enamecard.model.request.CreateENameCardRequest;
import com.demo.enamecard.model.response.ENameCardResponse;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface ENameCardMapper{
    ENameCardMapper INSTANCE = Mappers.getMapper(ENameCardMapper.class);

    ENameCardResponse map(ENameCardEntity eNameCardEntity);

    List<ENameCardResponse> mapList(List<ENameCardEntity> eNameCardEntities);

    ENameCardEntity map(CreateENameCardRequest createENameCardRequest);
}
